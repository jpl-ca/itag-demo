package secitag.jmt.com.securityitag.seci_res.receiver;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import secitag.jmt.com.securityitag.seci_res.service.BluetoothService;

/**
 * Created by JMTech-Android on 30/10/2015.
 */
public class LinkBackground extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent){
        final int bluetoothState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
        Log.d("BluetoothService", "bluetooth change state: " + bluetoothState);
        System.out.println("---->"+"bluetooth change state: " + bluetoothState);
//        if (Preferences.getKeyringUUID(context) != null && Preferences.getLinkBackgroundEnabled(context) && bluetoothState == BluetoothAdapter.STATE_ON) {
        if (bluetoothState == BluetoothAdapter.STATE_ON) {
            context.startService(new Intent(context, BluetoothService.class));
        }
    }
}