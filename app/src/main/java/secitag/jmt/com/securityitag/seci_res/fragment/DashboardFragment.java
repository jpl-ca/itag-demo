package secitag.jmt.com.securityitag.seci_res.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import secitag.jmt.com.securityitag.R;

/**
 * Created by JMTech-Android on 03/11/2015.
 */
public class DashboardFragment extends Fragment {
    @Bind(R.id.btnEncender) Button btnEncender;
    @Bind(R.id.btnConectar) Button btnConectar;
    @Bind(R.id.btnApagar) Button btnApagar;
    @Bind(R.id.txtBateria) TextView txtBateria;
    @Bind(R.id.txtRSSI) TextView txtRSSI;
    @Bind(R.id.txtEstado) TextView txtEstado;
    private OnDashboardListener presenter;

    public static DashboardFragment instance()
    {
        return new DashboardFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dash, container, false);
        ButterKnife.bind(this,rootView);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        if (activity instanceof OnDashboardListener) {
            this.presenter = (OnDashboardListener) activity;
        } else {
            throw new ClassCastException("debe implementar OnDashboardListener");
        }
    }

    @OnClick(R.id.btnConectar)
    public void Conectar(){
        this.presenter.onDashboardStarted();
    }

    @OnClick(R.id.btnEncender)
    public void Encender(){
        presenter.onStartAlert();
    }

    @OnClick(R.id.btnApagar)
    public void Apagar(){
        presenter.onStopAlert();
    }

    public void setPercent(final String percent){
        txtBateria.setText("Bateria: " + percent);
    }

    public void setRssi(String rssi){
        txtRSSI.setText("RSSI: "+rssi);
    }

    public void setImmediateAlertEnabled(final boolean enabled){
        txtEstado.setText((enabled)?"Conectado":"Desconectado");
    }

    @Override
    public void onStart(){
        super.onStart();
//        this.presenter.onDashboardStarted();//TODO INICIAR Conexion
    }

    @Override
    public void onStop(){
        super.onStop();
        this.presenter.onDashboardStopped();
    }

    public interface OnDashboardListener {
        void onStartAlert();
        void onRingAlert();
        void onStopAlert();

        void onLinkLoss(boolean checked);

        void onDashboardStarted();

        void onDashboardStopped();
    }
}