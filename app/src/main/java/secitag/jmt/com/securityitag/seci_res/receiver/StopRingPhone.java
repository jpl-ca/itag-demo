package secitag.jmt.com.securityitag.seci_res.receiver;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by JMTech-Android on 30/10/2015.
 */
public class StopRingPhone extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent)
    {
        if (StartRingPhone.currentRingtone != null) {
            StartRingPhone.currentRingtone.stop();
        }

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(StartRingPhone.NOTIFICATION_ID);
    }
}
