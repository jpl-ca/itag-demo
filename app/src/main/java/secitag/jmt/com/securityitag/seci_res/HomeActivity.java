package secitag.jmt.com.securityitag.seci_res;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import secitag.jmt.com.securityitag.R;
import secitag.jmt.com.securityitag.seci_res.fragment.DashboardFragment;
import secitag.jmt.com.securityitag.seci_res.service.BluetoothService;

/**
 * Created by JMTech-Android on 30/10/2015.
 */
public class HomeActivity extends Activity implements DashboardFragment.OnDashboardListener{
    private final static int REQUEST_ENABLE_BT = 1;
    private final String MAC_ADDRESS_BT = "FF:FF:60:00:00:D2";
    private final DashboardFragment dashboardFragment = DashboardFragment.instance();
    private BluetoothAdapter mBluetoothAdapter;
    private Handler mHandler;
    private BroadcastReceiver receiver;
    private BluetoothService service;
    private boolean isBound = false;;

    @Bind(R.id.swipe_refresh_layout) SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_home);
        ButterKnife.bind(this);
        getActionBar().setDisplayShowHomeEnabled(true);

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            finish();
        }

        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        mHandler = new Handler();
        showDashboard();

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, final Intent intent){
                if (BluetoothService.SERVICES_DISCOVERED.equals(intent.getAction())) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run(){
                            System.out.println("---------------------???????????");
                            setRefreshing(false);
                        }
                    });
                }
                if (BluetoothService.IMMEDIATE_ALERT_AVAILABLE.equals(intent.getAction())) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run(){
                            System.out.println("---------------------ALERTA_ACTIVADO");
                            dashboardFragment.setImmediateAlertEnabled(true);
                        }
                    });
                }
                if (BluetoothService.BATTERY_LEVEL.equals(intent.getAction())) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run(){
                            System.out.println("---------------------ENVIANDO BATERIA");
                            dashboardFragment.setPercent(intent.getStringExtra(BluetoothService.BATTERY_LEVEL));
                        }
                    });
                }
                if (BluetoothService.RSSI_RECEIVED.equals(intent.getAction())) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run(){
                            System.out.println("---------------------ENVIANDO RSSI");
                            dashboardFragment.setRssi(intent.getStringExtra(BluetoothService.RSSI_RECEIVED));
                        }
                    });
                }
            }
        };
    }

    private void showDashboard(){
        getFragmentManager().beginTransaction().replace(R.id.container, dashboardFragment).commit();
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            if (iBinder instanceof BluetoothService.BackgroundBluetoothBinder) {
                service = ((BluetoothService.BackgroundBluetoothBinder) iBinder).service();
                service.setMacAddress(MAC_ADDRESS_BT);
                service.connect();
                isBound = true;
                setRefreshing(true);
            }
        }
        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            service.disconnect();
            isBound = false;
            setRefreshing(false);
        }
    };

    @Override
    protected void onStart(){
        super.onStart();
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }


    @Override
    public void onStartAlert() {
        service.immediateAlert(BluetoothService.HIGH_ALERT);
    }

    @Override
    public void onRingAlert() {
        service.immediateAlert(BluetoothService.MEDIUM_ALERT);
    }

    @Override
    public void onStopAlert() {
        service.immediateAlert(BluetoothService.NO_ALERT);
    }

    @Override
    public void onLinkLoss(boolean checked) {
        //SI SE PIERDE LA CONEXIONM SE DETENDRA EL SERVICI (CREO)O
        final Intent service = new Intent(this, BluetoothService.class);
        if (checked) {
            startService(service);
        } else {
            stopService(service);
        }
    }

    @Override
    public void onDashboardStarted() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(BluetoothService.IMMEDIATE_ALERT_AVAILABLE));
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(BluetoothService.BATTERY_LEVEL));
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(BluetoothService.SERVICES_DISCOVERED));
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(BluetoothService.RSSI_RECEIVED));
        isBound = bindService(new Intent(this, BluetoothService.class), serviceConnection, BIND_AUTO_CREATE);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(){
                service.connect();
            }
        });
    }

    @Override
    public void onDashboardStopped() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        if(isBound)
        unbindService(serviceConnection);
    }

    private void setRefreshing(final boolean refreshing){
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run()
            {
                mSwipeRefreshLayout.setRefreshing(refreshing);
            }
        });
    }
}