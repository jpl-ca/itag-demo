package secitag.jmt.com.securityitag.seci_res.service;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import java.util.UUID;

import secitag.jmt.com.securityitag.R;
import secitag.jmt.com.securityitag.seci_res.MenuWithFABActivity;

/**
 * Created by JMTech-Android on 03/11/2015.
 */
public class BluetoothService extends Service{
    public static final int NO_ALERT = 0x00;
    public static final int MEDIUM_ALERT = 0x01;
    public static final int HIGH_ALERT = 0x02;

    public static final String IMMEDIATE_ALERT_AVAILABLE = "IMMEDIATE_ALERT_AVAILABLE";
    public static final String BATTERY_LEVEL = "BATTERY_LEVEL";
    public static final String GATT_CONNECTED = "GATT_CONNECTED";
    public static final String SERVICES_DISCOVERED = "SERVICES_DISCOVERED";
    public static final String RSSI_RECEIVED = "RSSI_RECEIVED";

    public static final UUID IMMEDIATE_ALERT_SERVICE = UUID.fromString("00001802-0000-1000-8000-00805f9b34fb");
    public static final UUID FIND_ME_SERVICE = UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb");
    public static final UUID LINK_LOSS_SERVICE = UUID.fromString("00001803-0000-1000-8000-00805f9b34fb");
    public static final UUID BATTERY_SERVICE = UUID.fromString("0000180f-0000-1000-8000-00805f9b34fb");
    public static final UUID CLIENT_CHARACTERISTIC_CONFIG = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    public static final UUID ALERT_LEVEL_CHARACTERISTIC = UUID.fromString("00002a06-0000-1000-8000-00805f9b34fb");
    public static final UUID FIND_ME_CHARACTERISTIC = UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb");

    public static final String VIBRATE_PHONE = "VIBRATE_PHONE";
    public static final String STOP_VIBRATE_PHONE = "STOP_VIBRATE_PHONE";
    public static final String RING_PHONE = "RING_PHONE";
    public static final String STOP_RING_PHONE = "STOP_RING_PHONE";
    public static final String CAPTURE_POSITION = "CAPTURE_POSITION";

    public static final String ACTION_PREFIX = "secitag.jmt.com.action.";
    private static final long DELAY_DOUBLE_CLICK = 750;
    private String macAddress = "";

    private BluetoothDevice mDevice;
    private BluetoothGatt bluetoothGatt = null;
    private BluetoothGattService immediateAlertService;
    private BluetoothGattCharacteristic batteryCharacteristic;
    private BluetoothGattCharacteristic buttonCharacteristic;
    private long lastChange;
    private Runnable r;
    private Handler handler = new Handler();

    private boolean IS_VIBRATING = false;
    private boolean IS_RINGING = false;

    private BackgroundBluetoothBinder myBinder = new BackgroundBluetoothBinder();
    private LocalBroadcastManager broadcaster;

    private void setCharacteristicNotification(BluetoothGatt bluetoothgatt, BluetoothGattCharacteristic bluetoothgattcharacteristic, boolean flag){
        bluetoothgatt.setCharacteristicNotification(bluetoothgattcharacteristic, flag);
        if (FIND_ME_CHARACTERISTIC.equals(bluetoothgattcharacteristic.getUuid())) {
            BluetoothGattDescriptor descriptor = bluetoothgattcharacteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG);
            if (descriptor != null) {
                descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                bluetoothgatt.writeDescriptor(descriptor);
            }
        }
    }
    public void enablePeerDeviceNotifyMe(BluetoothGatt bluetoothgatt, boolean flag){
        BluetoothGattCharacteristic bluetoothgattcharacteristic = getCharacteristic(bluetoothgatt, FIND_ME_SERVICE, FIND_ME_CHARACTERISTIC);
        if (bluetoothgattcharacteristic != null && (bluetoothgattcharacteristic.getProperties() | 0x10) > 0) {
            setCharacteristicNotification(bluetoothgatt, bluetoothgattcharacteristic, flag);
        }
    }
    private BluetoothGattCharacteristic getCharacteristic(BluetoothGatt bluetoothgatt, UUID serviceUuid, UUID characteristicUuid){
        if (bluetoothgatt != null) {
            BluetoothGattService service = bluetoothgatt.getService(serviceUuid);
            if (service != null)
                return service.getCharacteristic(characteristicUuid);
        }
        return null;
    }
    private BluetoothGattCallback bluetoothGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState){
            System.out.println("<<<<<<AA.......!!!"+status+"|"+newState);
            System.out.println("Cambio de estado---------->ST:"+status+"->NST:"+newState);
            if (BluetoothGatt.GATT_SUCCESS == status) {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    broadcaster.sendBroadcast(new Intent(GATT_CONNECTED));
                    gatt.discoverServices();
                }
            }
//            final boolean actionOnPowerOff = Preferences.isActionOnPowerOff(BluetoothService.this);
//            if (actionOnPowerOff || status == 8) {
//                if (newState == BluetoothProfile.STATE_DISCONNECTED) {
//                    String action = Preferences.getActionOutOfBand(getApplicationContext());
//                    sendBroadcast(new Intent(ACTION_PREFIX + action));
//                    enablePeerDeviceNotifyMe(gatt, false);
//                }
//            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status){
            System.out.println("<<<<<<BB.......!!!"+status);
            gatt.readRemoteRssi();
            broadcaster.sendBroadcast(new Intent(SERVICES_DISCOVERED));
            if (BluetoothGatt.GATT_SUCCESS == status) {
                for (BluetoothGattService service : gatt.getServices()) {
                    if (IMMEDIATE_ALERT_SERVICE.equals(service.getUuid())) {
                        immediateAlertService = service;
                        broadcaster.sendBroadcast(new Intent(IMMEDIATE_ALERT_AVAILABLE));
                        gatt.readCharacteristic(getCharacteristic(gatt, IMMEDIATE_ALERT_SERVICE, ALERT_LEVEL_CHARACTERISTIC));
                    }
                    if (BATTERY_SERVICE.equals(service.getUuid())) {
                        batteryCharacteristic = service.getCharacteristics().get(0);
                        gatt.readCharacteristic(batteryCharacteristic);
                    }
                    if (FIND_ME_SERVICE.equals(service.getUuid())) {
                        if (!service.getCharacteristics().isEmpty()) {
                            buttonCharacteristic = service.getCharacteristics().get(0);
                            setCharacteristicNotification(gatt, buttonCharacteristic, true);
                        }
                    }
                }
                enablePeerDeviceNotifyMe(gatt, true);
            }
        }
        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic){
            System.out.println("<<<<<<CC.......!!!");
            final long now = SystemClock.elapsedRealtime();
            final String str = new String(characteristic.getValue());
            System.out.println((lastChange) +"|"+ (2 * DELAY_DOUBLE_CLICK > now)+"| TRUE???->"+(lastChange + 2 * DELAY_DOUBLE_CLICK > now));
            if (lastChange + DELAY_DOUBLE_CLICK > now) {
                handler.removeCallbacks(r);
                r = new Runnable() {
                    @Override
                    public void run()
                    {
                        lastChange = 0;
//                        String action = IS_RINGING?STOP_RING_PHONE:RING_PHONE;
//                        IS_RINGING = !IS_RINGING;
//                        sendBroadcast(new Intent(ACTION_PREFIX + action));
//                        System.out.println("*************************************TWO-A---->"+str+"|"+ACTION_PREFIX + action);
                        Intent dialogIntent = new Intent(BluetoothService.this, MenuWithFABActivity.class);
                        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(dialogIntent);
                    }
                };
                handler.postDelayed(r, DELAY_DOUBLE_CLICK);
            } else if(lastChange + 2 * DELAY_DOUBLE_CLICK > now){
                lastChange = 0;
                handler.removeCallbacks(r);
                String action = CAPTURE_POSITION;
                sendBroadcast(new Intent(ACTION_PREFIX + action));
                System.out.println("*************************************TWO-C---->"+str+"|"+ACTION_PREFIX + action);
            } else if(lastChange + 3 * DELAY_DOUBLE_CLICK > now){
                lastChange = 0;
                handler.removeCallbacks(r);
                String action = CAPTURE_POSITION;
                sendBroadcast(new Intent(ACTION_PREFIX + action));
                System.out.println("*************************************TWO-C---->"+str+"|"+ACTION_PREFIX + action);
            } else {
                lastChange = now;
                r = new Runnable() {
                    @Override
                    public void run()
                    {
                        String action = IS_VIBRATING?STOP_VIBRATE_PHONE:VIBRATE_PHONE;
                        IS_VIBRATING = !IS_VIBRATING;
                        sendBroadcast(new Intent(ACTION_PREFIX + action));
                        System.out.println("*************************************ONE---->"+str+"|"+ACTION_PREFIX + action);
                    }
                };
                handler.postDelayed(r, DELAY_DOUBLE_CLICK);
            }
        }



        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status){
            String str = new String(characteristic.getValue());
            System.out.println("*************************************ENVIANDO INFO DE LA BATERIA->"+str+"->"+status);
            final Intent batteryLevel = new Intent(BATTERY_LEVEL);
            batteryLevel.putExtra(BATTERY_LEVEL, Integer.valueOf(characteristic.getValue()[0]) + "%");
            broadcaster.sendBroadcast(batteryLevel);
        }
        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            System.out.println("*************************************ENVIANDO INFO DE RSSI->" + status);
            final Intent rssiIntent = new Intent(RSSI_RECEIVED);
            rssiIntent.putExtra(RSSI_RECEIVED, rssi + "dBm");
            broadcaster.sendBroadcast(rssiIntent);
        }
        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status){
            System.out.println("*************************************CCCC"+status);
            gatt.readCharacteristic(batteryCharacteristic);
        }
    };

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public class BackgroundBluetoothBinder extends Binder {
        public BluetoothService service(){
            return BluetoothService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        this.connect();
        return START_STICKY;
    }
    @Override
    public void onCreate(){
        super.onCreate();
        this.broadcaster = LocalBroadcastManager.getInstance(this);
    }
    @Override
        public void onDestroy(){
        super.onDestroy();
    }

    public void connect(){
        System.out.println("<<<bluetoothGatt is ->"+this.bluetoothGatt);
        if (this.bluetoothGatt == null) {
            System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
            System.out.println("<<<connect()  -  connecting GATT>>>");
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>"+macAddress);
            mDevice = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(macAddress);
            System.out.println("<<<<<<Conectando.......!!!");
            this.bluetoothGatt = mDevice.connectGatt(this, true, bluetoothGattCallback);
            System.out.println("<<<<<<Esperando.......!!!");
        } else {
            System.out.println("connect() - discovering services");
            this.bluetoothGatt.discoverServices();
        }
    }

    public void immediateAlert(int type){
        if (immediateAlertService == null || immediateAlertService.getCharacteristics() == null || immediateAlertService.getCharacteristics().size() == 0) {
            somethingGoesWrong();
            return;
        }
        final BluetoothGattCharacteristic characteristic = immediateAlertService.getCharacteristics().get(0);
        characteristic.setValue(type, BluetoothGattCharacteristic.FORMAT_UINT8, 0);
        this.bluetoothGatt.writeCharacteristic(characteristic);
    }

    public void disconnect(){
        this.bluetoothGatt.disconnect();
        this.bluetoothGatt.close();
        this.bluetoothGatt = null;
    }
    private void somethingGoesWrong(){
        Toast.makeText(this, R.string.something_goes_wrong, Toast.LENGTH_LONG).show();
    }
}